import React from 'react';

export default function HomeContainer() {
	return (
		<div className="homeContainer">
			<h1 className="flexItem0">Home</h1>
			<div className="flexImage">
				<img src="kittens.jpg" alt="kitteh"></img>
			</div>
			<p className="flexItem0">Photo by <a href="https://freeimages.com/photographer/coralsea-54473">Carol Kramberger</a> from <a href="https://freeimages.com/">FreeImages</a></p>
		</div>
	);
}
