import React, {useState, useEffect, Fragment} from 'react'
import Plot from 'react-plotly.js'
import axios from 'axios'
axios.defaults.baseURL = 'http://localhost:8000'

export default function PlotsContainer() {
	const [values1, setValues1] = useState(null)
	const [values2, setValues2] = useState(null)

	// Get all values from a list of {key: value} objects
	const getValues = (res) => {
		var values = []
		for (var d in res.data)
			values.push(Object.values(res.data[d])[0])

		return values
	}

	// Get X axis index array to match Y axis data array
	const getX = (y) => {
		var x = []
		for (var i=1; i <= y.length; i++)
			x.push(i)
		return x
	}

	// Constructor
	useEffect(() => {
		//Load values from database
		axios.post('/getValues', {index:1})
		.then(res =>{
			setValues1(getValues(res))
			console.log('Loaded values 1')
		})
		.catch(err => {console.log(err)})

		axios.post('/getValues', {index:2})
		.then(res =>{
			setValues2(getValues(res))
			console.log('Loaded values 2')
		})
		.catch(err => {console.log(err)})
	}, []);

	/* Button callbacks */

	const addValues = () => {
		axios.post('/addData', {data:[
			null, // MySQL requirement. Could be added by the back-end instead
			'Test',
			new Date().toISOString().substring(0, 10),
			false,
			Math.random(), 1+Math.random()]
		})
		.then(res =>{
			console.log('Added data')
			window.location.reload(false);
		})
		.catch(err => {console.log(err)})
	}

	const removeValues = () => {
		axios.post('/removeData')
		.then(res =>{
			console.log('Removed data')
			window.location.reload(false);
		})
		.catch(err => {console.log(err)})
	}

	return (
		<Fragment>
		{/* Show plot once data has been loaded*/}
		{values1 === null || values2 === null ? null :
			<Fragment>
				<h1>Plotting</h1>
				<Plot
					data={[{
							y: values1,
							x: getX(values1),
							name: 'Values 1',
							type: 'scatter',
							mode: 'lines+markers',
							marker: {color: 'red'},
						}, {
							y: values2,
							x: getX(values2),
							name: 'Values 2',
							type: 'scatter',
							mode: 'lines',
							marker: {color: 'blue'},
						},
					]}
					layout={{
						title: 'Test plot',
						width: 1000, height: 600,
						/** Transparent background color **/
						paper_bgcolor:'rgba(0,0,0,0)',
    				plot_bgcolor:'rgba(0,0,0,0)',
						/*Set initial dragmode to pan instead of zoom*/
						dragmode: 'pan'
					}}

					config={{
						scrollZoom: true,
						/** Mode bar setup **/
						displaylogo: false,
						modeBarButtonsToRemove: ['select2d', 'lasso2d', 'zoom2d', 'zoomIn2d', 'zoomOut2d', 'autoScale2d']
					}}
				/>
				<div className='centerContainer'>
					<button className='button' onClick={addValues}>Add values</button>
					<button className='button' onClick={removeValues}>Remove values</button>
				</div>
			</Fragment>
		}
	</Fragment>
	);
}
