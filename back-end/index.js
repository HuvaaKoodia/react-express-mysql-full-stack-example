const express = require('express')
const mysql = require('mysql')
const cors = require('cors')
const fs = require('fs')

//Shared module example
const sharedModule = require('SharedModule')
sharedModule.Hello('back-end user')

const app = express()
const port = 8000

// Use cross-origin resource sharing
app.use(cors())

// Use automatic json serialization for requests and responses
app.use(express.json())

// Read configuration file and initialize MySQL pool
var configuration = null
var pool = null

fs.readFile("configuration.json", (err, data)=>{
	configuration = JSON.parse(data);

	pool = mysql.createPool({
		connectionLimit: 10,
		host: configuration.host,
		user: configuration.user,
		password: configuration.password,
		database: configuration.database
	});
})

// Logs a request notification for debugging (e.g. GET /url date)
app.use((req, res, next) => {
	console.log(req.method, req.url, new Date().toISOString())
	next()
})

/** Requests **/

app.post('/getValues', (req, res, next) => {
	var sql = 'SELECT value_' + req.body.index + ' FROM Test'

	pool.query(sql, [], (err, result) => {
		if (err)
			return next(err) // Forward to error handler
		res.status(200).json(result)
	});
})

app.post('/addData', (req, res) => {
	var values = req.body.data
	var sql = 'INSERT INTO Test (id, test_name, add_date, success, value_1, value_2) VALUES (?)'

	pool.query(sql, [values], (err, result) => {
		if (err)
			return next(err) // Forward to error handler
		res.status(200).json(result)
	})
})

app.post('/removeData', (req, res) => {
	// Delete last row
	var sql = 'delete from Test order by id desc limit 1'

	pool.query(sql, [], (err, result) => {
		if (err)
			return next(err) // Forward to error handler
		res.status(200).json(result)
	})
})

// Start server
var server = app.listen(port, () =>
	console.log('Back-end listening at http://localhost:' + port)
)

// Request error handler
app.use((err, req, res, next) => {
	console.log('Error 500:', err.message)
  res.status(500).send('Request failed')
})

// Uncaught exceptions log and close the server. This releases the port properly.
process.on('uncaughtException', function (err) {
	console.log('Uncaught exception:', err.message)
  server.close()
})
